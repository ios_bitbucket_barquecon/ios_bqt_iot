//
//  BQTAllDeviceCollectionViewCell.swift
//  BQT_IOT
//
//  Created by SCISPLMAC on 09/08/19.
//  Copyright © 2019 SCISPLMAC. All rights reserved.
//

import UIKit

class BQTAllDeviceCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var switchOnOffButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
