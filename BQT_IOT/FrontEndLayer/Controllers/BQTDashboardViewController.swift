//
//  BQTDashboardViewController.swift
//  BQT_IOT
//
//  Created by SCISPLMAC on 09/08/19.
//  Copyright © 2019 SCISPLMAC. All rights reserved.
//

import UIKit
import Alamofire
class BQTDashboardViewController: BQTBaseViewController {

    @IBOutlet weak var deviceListCollectionView: UICollectionView!
    var allDeviceArray = [[String:AnyObject]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        deviceListCollectionView.register(UINib(nibName: "BQTAllDeviceCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BQTAllDeviceCollectionViewCell")
     
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        if let userId  = UserDefaults.standard.value(forKey: "userId") as? String, let name  = UserDefaults.standard.value(forKey: "name") as? String {
            print("userId",userId)
            print("userId",name)
            self.getAllDevice(userId)
            deviceListCollectionView.reloadData()
            self.title = name
        }
    }

    @IBAction func logoutButtonAction(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "isLogin")
        UserDefaults.standard.removeObject(forKey: "id")
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.makeRootViewController()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BQTDashboardViewController : UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allDeviceArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BQTAllDeviceCollectionViewCell", for: indexPath) as! BQTAllDeviceCollectionViewCell
        cell.layer.borderColor = UIColor.black.cgColor
        cell.layer.borderWidth = 1.0
        
        if let name = allDeviceArray[indexPath.row]["name"] as? String ,let key = allDeviceArray[indexPath.row]["key"] as? String{
            if name.isEmpty {
                cell.nameLabel.text = key
            }else {
             cell.nameLabel.text = name
            }
            cell.moreButton.tag = indexPath.row
            cell.moreButton.addTarget(self, action: #selector(moreButtonAction), for: .touchUpInside)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  50
        let collectionViewSize = collectionView.frame.size.width - padding
        return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
    }
    
    @objc func moreButtonAction (_ sender : UIButton){
        print(sender.tag)
        if  let name = allDeviceArray[sender.tag]["name"] as? String ,let switch_Key = allDeviceArray[sender.tag]["switchKey"] as? String , let key = allDeviceArray[sender.tag]["switchKey"] as? String{
            let alertController = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
            let rename = UIAlertAction(title: "Rename", style: .default, handler: {
                alert -> Void in
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "BQTRenameDeviceViewController") as! BQTRenameDeviceViewController
                vc.modalPresentationStyle = .overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
                if name.isEmpty {
                    vc.deviceName = key
                }else{
                    vc.deviceName = name
                }
                vc.switchKey = switch_Key
                self.present(vc, animated: true, completion: nil)
            })
            
            let setImage = UIAlertAction(title: "set Image", style: .default, handler: {
                (action : UIAlertAction!) -> Void in
            })
            let timer = UIAlertAction(title: "Timer", style: .default, handler: {
                (action : UIAlertAction!) -> Void in
            })
            let schedule = UIAlertAction(title: "Schedule", style: .default, handler: {
                (action : UIAlertAction!) -> Void in
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
                (action : UIAlertAction!) -> Void in
            })
            alertController.addAction(rename)
            alertController.addAction(setImage)
            alertController.addAction(timer)
            alertController.addAction(schedule)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
}
extension BQTDashboardViewController {
    func getAllDevice(_ userId : String){
        let urlString = String(format: "%@%@", BQTConstatnts.baseURL,BQTConstatnts.getDeviceDetailsByUserId)
        print(urlString)
        Alamofire.request(urlString, method: .post, parameters: ["userId": userId], encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success :
                //print("Jokes_response",response)
                guard let res = response.result.value else {return}
                
                let status = (res as AnyObject).value(forKey: "status") as! Bool
                let message = (res as AnyObject).value(forKey: "message") as! String
                if status == true {
                    let data = (res as AnyObject).value(forKey: "userObject") as! [[String: AnyObject]]
                    self.allDeviceArray = data
                  //  print(data,"data")
                    self.deviceListCollectionView.reloadData()
                }else {
                    self.alertUser("Alert", message: message)
                }
                print(status)
                print(message)
            case .failure(let error):
                print(error)
                self.alertUser("Error", message: "Server seems to be down")
            }
        }
    }
}
