//
//  BQTSignUpViewController.swift
//  BQT_IOT
//
//  Created by SCISPLMAC on 08/08/19.
//  Copyright © 2019 SCISPLMAC. All rights reserved.
//

import UIKit
import Alamofire
class BQTSignUpViewController: BQTBaseViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    

    @IBAction func signUpButtonAction(_ sender: Any) {
        if nameTextField.text!.isEmpty {
            alertUser("Alert", message: "Enter name")
        } else if emailTextField.text!.isEmpty || !BQTUtilities.isValidEmail(testStr: emailTextField.text!) {
            alertUser("Alert", message: "Enter valid email")
        }else if mobileNumberTextField.text!.isEmpty {
            alertUser("Alert", message: "Enter mobile number")
        }else if passwordTextField.text!.isEmpty {
            alertUser("Alert", message: "Enter valid email")
        } else {
            self.signUp()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BQTSignUpViewController {
    func signUp(){
        let urlString = String(format: "%@%@", BQTConstatnts.baseURL,BQTConstatnts.registerUser)
        print(urlString)
        let param = ["name": nameTextField.text!,
                     "email":emailTextField.text!,
                     "mobileNo":mobileNumberTextField.text!,
                     "password":passwordTextField.text!,
                     "address" : "Pune",
                     "type" : "1"
        ]
        print(param)
        Alamofire.request(urlString, method: .post, parameters: param , encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success :
                print("Jokes_response",response)
                guard let res = response.result.value else {return}
                
                let status = (res as AnyObject).value(forKey: "status") as! Bool
                let message = (res as AnyObject).value(forKey: "message") as! String
                if status == true {
                    let data = (res as AnyObject).value(forKey: "userObject") as! [String: AnyObject]
                    print(data,"data")
                }else {
                    self.alertUser("Alert", message: message)
                }
                print(status)
                print(message)
            case .failure(let error):
                print(error)
                self.alertUser("Error", message: "Server seems to be down")
            }
            
        }
    }
}
