//
//  BQTRenameDeviceViewController.swift
//  BQT_IOT
//
//  Created by SCISPLMAC on 09/08/19.
//  Copyright © 2019 SCISPLMAC. All rights reserved.
//

import UIKit
import Alamofire
class BQTRenameDeviceViewController: BQTBaseViewController {
    var deviceName = ""
    var  switchKey = ""
    @IBOutlet weak var nameTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTextField.text = deviceName
        print(switchKey)
        // Do any additional setup after loading the view.
    }
    @IBAction func cancelButtonAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    

    @IBAction func renameButtonAction(_ sender: Any) {
        if nameTextField.text!.isEmpty {
             self.alertUser("Alert", message: "Please enter device name")
        }else {
                renameDeviceName(nameTextField.text!, switchKey: switchKey)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BQTRenameDeviceViewController {
    func renameDeviceName(_  deviceNmae: String, switchKey : String){
        let urlString = String(format: "%@%@", BQTConstatnts.baseURL,BQTConstatnts.renameSwitchKeyName)
        print(urlString)
        Alamofire.request(urlString, method: .post, parameters: ["switchKey":switchKey ,"name":deviceName], encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success :
                print("Jokes_response",response)
                guard let res = response.result.value else {return}
                
                let status = (res as AnyObject).value(forKey: "status") as! Bool
                let message = (res as AnyObject).value(forKey: "message") as! String
                if status == true {
                    self.getAllDevice()
                     self.alertUser("Alert", message: message)
                    //self.dismiss(animated: true, completion: nil)
                }else {
                    self.alertUser("Alert", message: message)
                }
                print(status)
                print(message)
            case .failure(let error):
                print(error)
                self.alertUser("Error", message: "Server seems to be down")
            }
        }
    }
    func getAllDevice(){
        let urlString = String(format: "%@%@", BQTConstatnts.baseURL,BQTConstatnts.getDeviceDetailsByUserId)
        print(urlString)
        Alamofire.request(urlString, method: .post, parameters: ["userId": "8"], encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success :
                //print("Jokes_response",response)
                guard let res = response.result.value else {return}
                
                let status = (res as AnyObject).value(forKey: "status") as! Bool
                let message = (res as AnyObject).value(forKey: "message") as! String
                if status == true {
                   // let data = (res as AnyObject).value(forKey: "userObject") as! [[String: AnyObject]]
                    //self.allDeviceArray = data
                    //  print(data,"data")
                   // self.deviceListCollectionView.reloadData()
                }else {
                    self.alertUser("Alert", message: message)
                }
                print(status)
                print(message)
            case .failure(let error):
                print(error)
                self.alertUser("Error", message: "Server seems to be down")
            }
        }
    }
}
