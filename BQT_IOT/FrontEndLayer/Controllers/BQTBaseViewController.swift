//
//  BQTBaseViewController.swift
//  BQT_IOT
//
//  Created by SCISPLMAC on 09/08/19.
//  Copyright © 2019 SCISPLMAC. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
class BQTBaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    class func valiadatePhoneNumber(text: String?) -> Bool {
        let PHONE_REGEX = "^[0-9]{10,}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: text)
        return result
    }
    
    class func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    func alertUser(_ title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BQTBaseViewController: NVActivityIndicatorViewable {
    func startAnimatingActivityIndicator() {
        let width = self.view.bounds.width / 3
        let height = width
        let size = CGSize(width: width, height: height)
        startAnimating(size, message: "Loading...", type: NVActivityIndicatorType.ballSpinFadeLoader, color: UIColor.white)
    }
    
    func stopAnimatingActivityIndicator() {
        self.stopAnimating()
    }
}
