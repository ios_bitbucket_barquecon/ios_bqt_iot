//
//  BQTLoginViewController.swift
//  BQT_IOT
//
//  Created by SCISPLMAC on 08/08/19.
//  Copyright © 2019 SCISPLMAC. All rights reserved.
//

import UIKit
import  Alamofire
class BQTLoginViewController: BQTBaseViewController {

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }
    
    @IBAction func loginButtonAction(_ sender: Any) {
        if mobileNumberTextField.text!.isEmpty {
            alertUser("Alert", message: "Enter mobile number")
        } else if passwordTextField.text!.isEmpty {
            alertUser("Alert", message: "Enter password")
        }else {
           login(mobileNumberTextField.text!, password: passwordTextField.text!)
        }
    }
    
    @IBAction func signUpButtonAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "BQTSignUpViewController") as! BQTSignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
        //login1()
    }
    @IBAction func forgotPasswordButtonAction(_ sender: Any) {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BQTLoginViewController {
    func login(_  userName: String , password : String){
        let urlString = String(format: "%@%@", BQTConstatnts.baseURL,BQTConstatnts.loginAPIConstant)
        print(urlString)
            Alamofire.request(urlString, method: .post, parameters: ["mobileNo": userName,"password":password], encoding: JSONEncoding.default).responseJSON { response in
                switch response.result {
                case .success :
                    print("Jokes_response",response)
                    guard let res = response.result.value else {return}
                    
                    let status = (res as AnyObject).value(forKey: "status") as! Bool
                    let message = (res as AnyObject).value(forKey: "message") as! String
                    if status == true {
                         let data = (res as AnyObject).value(forKey: "userObject") as! [String: AnyObject]
                            print(data,"data")
                        
                        UserDefaults.standard.set(true, forKey: "isLogin")
                        //print("Login Successfully")
                        UserDefaults.standard.synchronize()
                        let dashBoardVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBar") as! UITabBarController
                        self.navigationController?.pushViewController(dashBoardVC, animated: true)
                        if let userId = data["id"] as? String {
                            UserDefaults.standard.set(userId, forKey: "userId")
                            UserDefaults.standard.synchronize()
                            print(userId)
                        }
                        
                        if let name = data["name"] as? String{
                            UserDefaults.standard.set(name, forKey: "name")
                            UserDefaults.standard.synchronize()
                        }
                    }else {
                        self.alertUser("Alert", message: message)
                    }
                    print(status)
                    print(message)
                case .failure(let error):
                    print(error)
                    self.alertUser("Error", message: "Server seems to be down")
                }
        }
    }
    func login1(){
        let urlString = String(format: "%@%@", BQTConstatnts.baseURL,BQTConstatnts.registerUser)
        print(urlString)
        let param = ["name": "7875331314",
                     "email":"admin",
                     "mobileNo":"7875331314",
                     "password":"admin",
                     "address" : "Pune",
                     "type" : "1"
        ]
        Alamofire.request(urlString, method: .post, parameters: param , encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success :
                print("Jokes_response",response)
            case .failure(let error):
                print(error)
            }
            
        }
    }
}

extension BQTLoginViewController : UITextFieldDelegate {
    
}
