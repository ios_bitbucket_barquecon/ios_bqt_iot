//
//  BQTButton.swift
//  BQT_IOT
//
//  Created by SCISPLMAC on 09/08/19.
//  Copyright © 2019 SCISPLMAC. All rights reserved.
//

import UIKit
import Foundation
@IBDesignable class BQTButton: UIButton {
    
    override init (frame: CGRect) {
        super.init(frame : frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    @IBInspectable var leftImageDesignable: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
}
